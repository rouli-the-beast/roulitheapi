package fr.roulithebeast.roulitheapi.security;

import fr.roulithebeast.roulitheapi.RouliTheApp;
import fr.roulithebeast.roulitheapi.utils.HttpLogUtils;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.RoutingContext;

public class TokenApiAuthHandler implements Handler<RoutingContext>{
	
	private SecureOptions options;
	
	public TokenApiAuthHandler(SecureOptions options) {
		this.options = options;
	}
	
	@Override
	public void handle(RoutingContext ctx) {
		Route route = ctx.currentRoute();
		if(route.getPath().equals(this.options.getPath()) && route.methods().contains(this.options.getMethod())) {
			HttpServerRequest request = ctx.request();
			final String param = "api-key";
			if(request.params().contains(param) && TokenManager.get().checkTokenValidity(request.getParam(param))) {
				
				// ok
				ctx.next();
				
			}else {
				// authentification needed 401
				RouliTheApp.LOGGER.warn(HttpLogUtils.logHttpRequestRemote(request) + " auth error !");
				final JsonObject errorJsonResponse = new JsonObject();
				errorJsonResponse.put("error", "Authentification Error !");

				ctx.response()
				.setStatusCode(401)
				.putHeader("content-type", "application/json")
				.end(Json.encode(errorJsonResponse));
			}			
		}else {
			RouliTheApp.LOGGER.error("Route matching error !");
		}
	}	
	
	public SecureOptions getOptions() {
		return options;
	}
	
}
