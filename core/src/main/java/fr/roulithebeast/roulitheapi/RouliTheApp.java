package fr.roulithebeast.roulitheapi;

import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import fr.roulithebeast.roulitheapi.security.TokenManager;
import fr.roulithebeast.roulitheapi.sql.SqlConnector;  // Import the IOException class to handle errors
import fr.roulithebeast.roulitheapi.utils.MissingPropertyException;

public class RouliTheApp {
	
	public static final Logger LOGGER = LogManager.getLogger(RouliTheApp.class);
	
	public static void main(String[] args) {

		Thread currentThread = Thread.currentThread();
		currentThread.setName(RouliTheApp.class.getSimpleName());
		
		LOGGER.info("Starting App...");
		
		LOGGER.debug("SQL init...");
		SqlConnector connector;
		try {
			connector = new SqlConnector();
		} catch (ClassNotFoundException | MissingPropertyException | SQLException | IOException e) {
			LOGGER.error("SqlConnector Exception !", e);
			System.exit(1);
			return;
		}
		
		LOGGER.debug("TokenManager init...");
		try {
			TokenManager.init();
		} catch (DecodeException | IOException e) {
			LOGGER.error("TokenManager Exception !", e);
			System.exit(1);
			return;
		}		
		
		LOGGER.debug("Vertx HTTP handler init...");
		final Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new MyApiVerticle(connector));

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() {
					LOGGER.info("Shutdown App...");
					vertx.close(); 
					LogManager.shutdown();				
			}
		},RouliTheApp.class.getSimpleName() + "-shuthook"));
	}
}