package fr.roulithebeast.roulitheapi.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

import fr.roulithebeast.roulitheapi.RouliTheApp;

public abstract class DefaultProperties extends Properties{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7474515633562421485L;

	public DefaultProperties() {
		super();
		try {

			File file = new File(this.filename());
			InputStream input = RouliTheApp.class.getClassLoader().getResourceAsStream(this.filename());
			
			if (input == null) {
				RouliTheApp.LOGGER.error("Sorry, unable to find " + this.filename() + " in JAR Resources");
				return;
			}
			
			if(!file.exists()) {
				// create file
				Files.copy(input, file.toPath());
				RouliTheApp.LOGGER.debug( this.filename() + " successfuly imported from JAR Resources !");
			}

			input = new FileInputStream(file);
			this.load(input);

		} catch (IOException e) {
			RouliTheApp.LOGGER.error("Exception during SqlProperties initialization !", e);
		}
	}
	
	public abstract String filename();
	
}
