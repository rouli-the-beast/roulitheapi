package fr.roulithebeast.roulitheapi.api;

import fr.roulithebeast.roulitheapi.api.exceptions.NullApiTokenException;

public final class RouliTheAPI {

	public final static RouliTheAPI getAPI() {
		return getAPI(null);		
	}
	
	public final static RouliTheAPI getAPI(String token) {
		return new RouliTheAPI(token);
	}
	
	private String token;
	
	private RouliTheAPI(String token) {
		this.token = token;
	}
	
	public String getToken() throws NullApiTokenException {
		if(token == null) {
			throw new NullApiTokenException();
		}
		return token;
	}
	
}
